# Compass configuration file.
require '/Users/steven/.rvm/gems/ruby-1.9.2-p320/gems/susy-1.0.8/lib/susy.rb'
#require '/Users/steven/.rvm/gems/ruby-1.9.2-p320/gems/sass-globbing-1.1.0/lib/sass-globbing.rb'
# We also support plugins and frameworks, please read the docs http://docs.mixture.io/preprocessors#compass

project_path = "/Users/steven/Frontend/cccs/"

# Important! change the paths below to match your project setup
css_dir = "assets/css" # update to the path of your css files.
sass_dir = "assets/sass" # update to the path of your sass files.
images_dir = "assets/img" # update to the path of your image files.
javascripts_dir = "assets/js" # update to the path of your script files.

line_comments = false # if debugging (or using Mixture chrome extension - set this to true)
cache = true
color_output = false # required for Mixture
